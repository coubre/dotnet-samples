﻿using System.IO;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Reflection;
using OpenQA.Selenium.Chrome;

namespace NUnitTestSample
{
    [TestFixture]
    public class BrowserTest
    {
        readonly string BaseURL = "https://www.qasymphony.com";
        private IWebDriver _webDriver;

        [SetUp]
        public void Initialize()
        {
        
            //_webDriver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            ChromeOptions op = new ChromeOptions();
			op.LeaveBrowserRunning = true;
			_webDriver = new ChromeDriver(op);
            _webDriver.Manage().Window.FullScreen();
        
            /*
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("headless");
            chromeOptions.AddArgument("--remote-debugging-port=36861");        
            chromeOptions.BinaryLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            var chromeDriverService = ChromeDriverService.CreateDefaultService();
            _webDriver = new ChromeDriver(chromeDriverService, chromeOptions);       
        
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments(new List<string>() {
                "--silent-launch",
                "--no-startup-window",
                "no-sandbox",
                "headless",});
           
            _webDriver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            _webDriver.Manage().Window.Maximize();
            */
            
        }

        /// <summary>
        /// Go to Tricentis website and navigate to qTest Launch product
        /// </summary>
        [Test]
        public void GotoqTestLaunch()
        {
            // go to Tricentis's Agile Test Management page
            _webDriver.Navigate().GoToUrl("https://www.tricentis.com/products/agile-dev-testing-qtest/agile-test-management-qtest-manager/");

            // click on Automation link
            //IWebElement elm = _webDriver.FindElement(By.CssSelector("#menu-item-51791 > a"));
            //elm.Click();
            
            //Verify Title
            var webTitle = _webDriver.Title;
            var expectedTitle = "qTest Manager – Test Case Management | Tricentis";
            
            Assert.That(webTitle == expectedTitle);

        }

        [TearDown]
        public void Cleanup()
        {
            _webDriver.Close();
            _webDriver.Quit();
        }
    }
}
