// sample Execute Command for executing .NET core test with node executor
const https = require('https');
const request = require('C:\\TestAutomation\\agentctl-2.3.6\\nodejs\\node_modules\\request\\request.js');
const fs = require('fs');
const path = require('path');
const { execSync } = require('child_process');

var standardHeaders = {
    'Content-Type': 'application/json',
    'Authorization': `bearer d44e31af-3f15-41b0-a83c-c4afbbeb107e`
};

let projectData = JSON.parse($PROJECT_DATA);
let testrunsList = JSON.parse($TESTRUNS_LIST);
let trTestSuite = testrunsList[0].parentId;
console.log(`TestSuite ID: ${trTestSuite}`);
let testrunProp = testrunsList[0].properties;

let prop = testrunProp.find(function(e) {
  return e.field_name == 'Environment'
});

let trEnv = prop.field_value_name;

console.log(`TestRun Environment: ${trEnv}`);

let tsURL = 'https://sademo.qtestnet.com/api/v3/projects/' + `${projectData.project_id}` + '/test-suites/' + `${trTestSuite}`;

console.log(`Test Suite URL: ${tsURL}`);

const URL = "https://jsonplaceholder.typicode.com/posts";

const options = {
  method: 'GET',
  headers: standardHeaders
};

https.get(tsURL,options, res => {
  //console.log('statusCode:', res.statusCode);
  //console.log('headers:', res.headers);

  res.on('data', (d) => {
    //process.stdout.write(d);
	let tsProp = JSON.parse(d);
	let tsName = tsProp.name;
	console.log(`Test Suite Name: ${tsName}`);
	
let tsChromeDriverProp = tsProp.properties.find(function(e) {
  return e.field_name == 'Chrome Driver Version'
});

let tsChromeDriverVersion = tsChromeDriverProp.field_value_name;

console.log(`Chrome Driver Version: ${tsChromeDriverVersion}`);


let isWin = process.platform == "win32";

// process.env.WORKING_DIR holds the value of Working Directory configured in Universal Agent,
// if you did not specify Working Directory in Universal Agent, make sure you enter it here
let SOLUTION_DIR = process.env.WORKING_DIR || ``;
// validate the existence of SOLUTION_DIR, it it does not exist, print error and stop execution
if (!fs.existsSync(SOLUTION_DIR)) {
  console.error('No working directory found.');
  return;
}

// change .NET core version to fit your need
let TARGET_DOTNETCORE_VERSION = '2.2';

// path to test project, change it to reflect yours
let TEST_PROJECT_NAME = 'NUnitTestSample';
let TEST_PROJECT_DIR = path.resolve(SOLUTION_DIR, 'DotnetCore', TEST_PROJECT_NAME);
let TEST_PROJECT_PATH = path.resolve(TEST_PROJECT_DIR, `${TEST_PROJECT_NAME}.csproj`);

// possible value for configuration: Debug or Release
let CONFIGURATION = 'Debug';
// we are going to execute published test
let PUBLISH_DIR = path.resolve(TEST_PROJECT_DIR, 'bin', CONFIGURATION, `netcoreapp${TARGET_DOTNETCORE_VERSION}`, 'publish');
// path to the test project output
let TEST_PROJECT_PUBLISHED_PATH = path.resolve(PUBLISH_DIR, `${TEST_PROJECT_NAME}.dll`);

// this is the full path to dotnet command
// make sure you change it to reflect your environment
let DOTNET_EXECUTABLE_PATH = isWin ? 'C:/Program Files/dotnet/dotnet.exe' : '/usr/local/bin/dotnet';

// by default, the result folder will be created at  ${SOLUTION_DIR}/TestResults
let RESULT_DIR = path.resolve(`${SOLUTION_DIR}`, 'TestResults');
// this is the path to XML test result
let PATH_TO_XML_RESULT = path.resolve(`${RESULT_DIR}`, 'Results.xml');

// delete result dir if it exists
if (fs.existsSync(RESULT_DIR)) {
  if (isWin) {
    execSync(`rmdir /s /q "${RESULT_DIR}"`);
  } else {
    execSync(`rm -rf "${RESULT_DIR}"`);
  }
}

// the shell command builder
var commandBuilder = [];
// set DOTNET_CLI_HOME environment var to working directory for dotnet command to work properly
if (!isWin) {
  commandBuilder.push(`export DOTNET_CLI_HOME="${SOLUTION_DIR}"`);
}

// execute `dotnet publish` command to publish the test project,
// the published results will be stored in PUBLISH_DIR
commandBuilder.push(`"${DOTNET_EXECUTABLE_PATH}" publish "${TEST_PROJECT_PATH}"`);

// copy the chromedriver to the PUBLISH_DIR dir for the Selenium test to run properly
// remove these commands if you're not running Selenium tests and so there will be no chromedriver in the output
let CHROME_DRIVER_NAME = isWin ? 'chromedriver.exe' : 'chromedriver';
let COPY_COMMAND = isWin ? 'mklink' : 'cp';
let CHROME_DRIVER_VERSION = `${tsChromeDriverVersion}`;
let CHROME_APP_DIR = `C:\\Program Files (x86)\\Google\\Chrome\\Application`;
let CHROME_DRIVER_PATH = path.join(`${CHROME_APP_DIR}`, `${CHROME_DRIVER_VERSION}\\chromedriver.exe`)
let CHROME_DRIVER_PUBLISH_PATH = path.join(PUBLISH_DIR, `chromedriver.exe`);

if (isWin) {
	commandBuilder.push(`DEL "${CHROME_DRIVER_PUBLISH_PATH}"`);
	commandBuilder.push(`${COPY_COMMAND} "${CHROME_DRIVER_PUBLISH_PATH}" "${CHROME_DRIVER_PATH}"`);
 } 
else {
	let PATH_TO_CHROME_DRIVER = path.resolve(TEST_PROJECT_DIR, 'bin', CONFIGURATION, `netcoreapp${TARGET_DOTNETCORE_VERSION}`, CHROME_DRIVER_NAME);
	commandBuilder.push(`${COPY_COMMAND} "${PATH_TO_CHROME_DRIVER}" "${PUBLISH_DIR}"`);
}

/**
 * Kicks off the test. What it does is to resolve the value of TESTCASES_AC variable and validate:
 * Case 1: if that variable TESTCASES_AC has value, meaning there is/are test run(s) being scheduled in qTest Manager
 *   -- for each test run being scheduled, finds and executes test method whose name matches that test run's automation content
 * Case 2: the value of TESTCASES_AC is empty, meaning no test runs being scheduled when the Universal Agent is executed in the first time
 *   -- executes all the tests within the project output DLL
 */
let testMethods = ($TESTCASES_AC && $TESTCASES_AC.trim() != '') ? `/Tests:${TESTCASES_AC}` : ``;
if (testMethods != '') {
  // run specific test methods, change the log file path if possible
  commandBuilder.push(`"${DOTNET_EXECUTABLE_PATH}" vstest "${TEST_PROJECT_PUBLISHED_PATH}" ${testMethods} --logger:"nunit;LogFilePath=${PATH_TO_XML_RESULT}"`);
} else {
  // run all tests, change the log file path if possible
  commandBuilder.push(`"${DOTNET_EXECUTABLE_PATH}" vstest "${TEST_PROJECT_PUBLISHED_PATH}" --logger:"nunit;LogFilePath=${PATH_TO_XML_RESULT}"`);
}

// the dotnet test runner will throw exception when there is an assertion failed and causes the 
// test exited with code 1. So we wrap the command execution in a try catch block to ensure the 
// execute command is fully executed and not exited with code 1 when there is exception thrown from the test
try {
  // build the shell command
  let command = isWin ? commandBuilder.join(' && ') : commandBuilder.join('\n');
  // execute the shell command
  execSync(command, { stdio: "inherit" });
} catch (err) {
  console.error(`*** Test execution error ***`);
  console.error(err.message || err);
  console.error(`*** End test execution error ***`);
}

  });

}).on('error', (e) => {
  console.error(e);
});